//
//  WeatherButton.swift
//  SwiftUi-App1
//
//  Created by Mohamed Saeed on 10/04/2022.
//

import SwiftUI
import Foundation

struct WeatherButton: View {
    var title:String
    var textColor:Color
    var backgroundColor:Color
    
    
    var body: some View {
        Text(title)
            .frame(width: 200, height: 50)
            .background(backgroundColor)
            .foregroundColor(textColor)
            .font(.system(size: 20,weight: .bold,design: .default))
            .cornerRadius(10.0)
    }
}

//
//  SwiftUi_App1App.swift
//  SwiftUi-App1
//
//  Created by Mohamed Saeed on 10/04/2022.
//

import SwiftUI

@main
struct SwiftUi_App1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
